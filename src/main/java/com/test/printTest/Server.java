package com.test.printTest;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetClientOptions;
import io.vertx.core.net.NetSocket;

/**
 * Created by pcjoshi on 8/15/16.
 * Email: prakashjoshiversion1@gmail.com
 */
public class Server extends AbstractVerticle {
    @Override
    public void start(){
        System.out.println("running main verticle");
        NetClientOptions options = new NetClientOptions();
        options.setConnectTimeout(1000);
        options.setSsl(true);
        options.setTrustAll(true);
        NetClient client = vertx.createNetClient(options);
        client.connect(631,"192.168.0.33",res -> {
           if (res.succeeded()){
               System.out.println("Connection successful");
               NetSocket socket = res.result();
               System.out.println("is socket ssl : " + socket.isSsl());
               try {
                   RandomAccessFile file = new RandomAccessFile("/home/pcjoshi/Downloads/CyberReceipt1464258682669.txt","r");
                   byte[] b = new byte[(int) file.length()];
                   file.readFully(b);
                   file.close();
                   Buffer buffer = Buffer.buffer(b);
                   System.out.println("writing output");
                   socket.write(buffer);
               }catch (Exception ex){
                    ex.printStackTrace();
               }
               socket.close();
           }else {
               System.out.println("failed to connect to the server");
               System.out.println("Cause : " + res.cause().getLocalizedMessage());
           }
        });
    }
}
